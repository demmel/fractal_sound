/*
 * Pattern2SndConvertor.cpp
 *
 *  Created on: Jun 30, 2013
 *      Author: david
 */

#include <fstream>
#include <sstream>
#include <iostream>
#include <stdexcept>

#include "Pattern2SndConverter.h"
#include "Pattern2AmplitudeConverter.h"
#include "string_ops.h"

namespace frctl_snd {

Pattern2SndConverter::Pattern2SndConverter(std::string fileName) {
	if(!fileName.empty()) readPropertyFile(fileName);
}

Pattern2SndConverter::~Pattern2SndConverter() {
}

void Pattern2SndConverter::readPropertyFile(std::string fileName) {
	std::ifstream propFile(fileName);

	if (!propFile.good()) {
		throw std::runtime_error("Could not open file");
	}

	std::string line;
	while (!propFile.eof()) {
		std::getline(propFile, line);
		string_reduce(line);
		if (line.empty())
			continue;

		std::stringstream str(line);
		int value;
		std::string name;
		SymbolProperty values;

		str >> name;

		while (!str.eof()) {
			str >> value;
			values.push_back(value);
		}

		properties[name] = values;
	}
}

int *Pattern2SndConverter::convert(FractalPattern &pattern) {
	for (auto it = pattern.getSymbols().begin(); it != pattern.getSymbols().end(); it++) {
		if (properties.find(*it) == properties.end())
			throw std::runtime_error("Not all symbol properties have values.");
	}

	return convertPattern(pattern);
}

Pattern2SndConverter* Pattern2SndConverterFactory::create(std::string type, std::string prop_file) {
	if (type == "SampleAmplitude") {
		return new Pattern2AmplitudeConverter(prop_file);
	} else {
		return nullptr;
	}
}

} /* namespace frctl_snd */


