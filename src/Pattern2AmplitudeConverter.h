/*
 * Pattern2FrequencyConvertor.h
 *
 *  Created on: Jun 30, 2013
 *      Author: david
 */

#ifndef PATTERN2FREQUENCYCONVERTOR_H_
#define PATTERN2FREQUENCYCONVERTOR_H_

#include "Pattern2SndConverter.h"

namespace frctl_snd {

class Pattern2AmplitudeConverter: public Pattern2SndConverter {
public:
	Pattern2AmplitudeConverter(std::string fileName = "");
	virtual ~Pattern2AmplitudeConverter();
private:
	virtual int *convertPattern(FractalPattern &pattern);
};

} /* namespace frctl_snd */
#endif /* PATTERN2FREQUENCYCONVERTOR_H_ */
