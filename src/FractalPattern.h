/*
 * FractalPattern.h
 *
 *  Created on: Jun 29, 2013
 *      Author: david
 */

#include "types.h"

#ifndef FRACTALPATTERN_H_
#define FRACTALPATTERN_H_

namespace frctl_snd {

class FractalPattern {
public:
	FractalPattern(std::string fileName);
	FractalPattern(SymbolSequence sequence, SymbolSet symbols, RuleSet rules);
	~FractalPattern();
	const SymbolSet& getSymbols() { return symbols; }
	const SymbolSequence& getCurrentSequence() { return currentSequence; }
	void iterate(int n=1);
private:
	SymbolSet symbols;
	RuleSet rules;
	SymbolSequence sequence;
	SymbolSequence currentSequence;
};

} /* namespace frctl_snd */

#endif /* FRACTALPATTERN_H_ */
