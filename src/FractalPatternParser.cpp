/*
 * RuleParser.cpp
 *
 *  Created on: Jun 29, 2013
 *      Author: david
 */

#include <sstream>
#include <iostream>

#include "FractalPatternParser.h"

namespace frctl_snd {

FractalPatternParser::FractalPatternParser() { }
FractalPatternParser::~FractalPatternParser() { }

void FractalPatternParser::parseLine(std::string line) {
	std::stringstream str(line);
	std::string symbol;
	if(sequence.empty()) {
		while(!str.eof()) {
			str >> symbol;
			sequence.push_back(symbol);
			symbols.insert(symbol);
		}
	} else {
		std::string from;
		SymbolSequence to;

		str >> from;
		str >> symbol;

		while(!str.eof()) {
			str >> symbol;
			to.push_back(symbol);
			symbols.insert(symbol);
		}

		rules[from] = to;
	}
}

} /* namespace frctl_snd */

