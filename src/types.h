/*
 * types.h
 *
 *  Created on: Jun 29, 2013
 *      Author: david
 */

#ifndef TYPES_H_
#define TYPES_H_

#include <string>
#include <list>
#include <unordered_set>
#include <map>

typedef std::string Symbol;
typedef std::list<Symbol> SymbolSequence;
typedef std::unordered_set<Symbol> SymbolSet;
typedef std::map<Symbol,SymbolSequence> RuleSet;

#endif /* TYPES_H_ */
