/*
 * main.cpp
 *
 *  Created on: Jun 29, 2013
 *      Author: david
 */

#include <iostream>
#include <fstream>
#include <sndfile.h>
#include <stdlib.h>
#include <stdexcept>

#include "string_ops.h"
#include "FractalPattern.h"
#include "Pattern2SndConverter.h"

using namespace std;
using namespace frctl_snd;

std::string fractal_file = "";
std::string prop_file = "";
int sample_rate = 96000;
int iterations = 5;
std::string converter_type = "SampleAmplitude";
std::string output_file = "out.wav";

void display_usage() {
	cerr << "fractal_sound - A sound generator based on fractals" << endl;
	cerr << "Usage: fractal_sound <fractal_file> [OPTIONS]" << endl;
	cerr << endl << "Options:" << endl;
	cerr << "\t-p <property_file>  - File to read converter properties from" << endl;
	cerr << "\t-s <sample_rate>    - Sample rate output file" << endl;
	cerr << "\t-t <converter_type> - Type of Pattern to Sound conversion" << endl;
	cerr << "\t-i <iterations>     - Ietrations to run the fractal pattern through" << endl;
	cerr << "\t-o <out_file>       - Name of file to output to" << endl;
	exit(EXIT_FAILURE);
}

void read_arguments(int argc, char *argv[]) {
	enum arg_state {
		NONE, PROP_FILE, SAMPLE_RATE, ITERATIONS, CONV_TYPE, OUT_FILE
	};

	int argi = 1;
	enum arg_state arg_type = NONE;
	string arg;

	while (argi < argc) {
		arg = argv[argi++];
		switch (arg_type) {
		case NONE:
			if ("-p" == arg)
				arg_type = PROP_FILE;
			else if ("-s" == arg)
				arg_type = SAMPLE_RATE;
			else if ("-t" == arg)
				arg_type = CONV_TYPE;
			else if ("-i" == arg)
				arg_type = ITERATIONS;
			else if ("-o" == arg)
				arg_type = OUT_FILE;
			else
				fractal_file = arg;
			break;
		case PROP_FILE:
			prop_file = arg;
			arg_type = NONE;
			break;
		case SAMPLE_RATE:
			sample_rate = atoi(arg.c_str());
			arg_type = NONE;
			break;
		case ITERATIONS:
			iterations = atoi(arg.c_str());
			arg_type = NONE;
			break;
		case CONV_TYPE:
			converter_type = arg;
			arg_type = NONE;
			break;
		case OUT_FILE:
			output_file = arg;
			arg_type = NONE;
			break;
		default:
			throw "Illegal Arugment State";
			break;
		}
	}

	if (arg_type != NONE) {
		throw std::runtime_error("flag '" + arg + "' without value");
	}
}

int main(int argc, char *argv[]) {
	try {
		if (argc < 2)
			display_usage();
		read_arguments(argc, argv);

		FractalPattern fp(fractal_file);
		fp.iterate(iterations);

		Pattern2SndConverter *p2sc = Pattern2SndConverterFactory::create(converter_type, prop_file);
		if (p2sc == nullptr) {
			throw std::runtime_error("Converter Type '" + converter_type + "' does not exist");
		}
		int *sound = p2sc->convert(fp);

		SF_INFO sfinfo;
		sfinfo.samplerate = sample_rate;
		sfinfo.channels = 2;
		sfinfo.format = SF_FORMAT_WAV | SF_FORMAT_PCM_32;
		SNDFILE * sndfile = sf_open(output_file.c_str(), SFM_WRITE, &sfinfo);

		sf_writef_int(sndfile, sound, fp.getCurrentSequence().size());

		sf_close(sndfile);

		delete[] sound;
		delete p2sc;
	} catch (std::exception &e) {
		cerr << "Error: " << e.what() << endl;
		exit(EXIT_FAILURE);
	}

	return 0;
}
