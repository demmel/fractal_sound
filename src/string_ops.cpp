/*
 * string_ops.cpp
 *
 *  Created on: Jun 30, 2013
 *      Author: david
 */

#include <iostream>

#include "string_ops.h"

bool string_trim(std::string &str) {
	bool trimmed = false;
	size_t newStart = str.find_first_not_of(" \t");
	if(newStart != 0) {
		str.erase(0, newStart);
		trimmed = true;
	}
	size_t newEnd = str.find_last_not_of(" \t");
	if(newEnd != str.length()-1) {
		str.erase(newEnd+1);
		trimmed = true;
	}
	return trimmed;
}

bool string_reduce(std::string &str) {
	bool reduced = string_trim(str);
	size_t pos = 0;
	while(str.npos != (pos = str.find_first_of(" \t", pos))) {
		size_t nextWordPos = str.find_first_not_of(" \t", pos);
		if(str.at(pos)=='\t' || (nextWordPos-pos)>1) {
			str.replace(pos, nextWordPos-pos, " ");
			reduced = true;
		}
		pos++;
	}
	return reduced;
}

void print_string_list(const std::list<std::string> &list) {
	for(auto it = list.begin(); it != list.end(); it++) {
		if(it == list.begin()) std::cout << *it;
		else std::cout << " " << *it;
	}
	std::cout << std::endl;
}

