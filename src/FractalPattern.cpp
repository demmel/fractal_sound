/*
 * FractalPattern.cpp
 *
 *  Created on: Jun 29, 2013
 *      Author: david
 */

#include <fstream>
#include <utility>
#include <iostream>
#include <stdexcept>

#include "FractalPattern.h"
#include "FractalPatternParser.h"
#include "string_ops.h"

namespace frctl_snd {
FractalPattern::FractalPattern(std::string fileName) {
	std::ifstream fractFile(fileName);

	if (!fractFile.good()) {
		throw std::runtime_error("Could not open file: " + fileName);
	}

	FractalPatternParser fpp;

	std::string line;
	while (!fractFile.eof()) {
		std::getline(fractFile, line);
		string_reduce(line);
		if (line.empty())
			continue;
		fpp.parseLine(line);
	}

	sequence.splice(sequence.begin(), fpp.getSequence());
	currentSequence = sequence;
	std::move(fpp.getSymbols().begin(), fpp.getSymbols().end(), std::inserter(symbols, symbols.begin()));
	std::move(fpp.getRules().begin(), fpp.getRules().end(), std::inserter(rules, rules.begin()));
}

FractalPattern::FractalPattern(SymbolSequence sequence, SymbolSet symbols, RuleSet rules) :
		symbols(symbols), rules(rules), sequence(sequence), currentSequence(sequence) {
}

FractalPattern::~FractalPattern() {
}

void FractalPattern::iterate(int n) {
	for (int i = 0; i < n; i++) {
		SymbolSequence nextSequence;
		for (auto it = currentSequence.begin(); it != currentSequence.end(); it++) {
			if (rules.find(*it) != rules.end())
				nextSequence.insert(nextSequence.end(), rules[*it].begin(), rules[*it].end());
			else
				nextSequence.push_back(*it);
		}
		currentSequence = nextSequence;
	}
}

} /* namespace frctl_snd */

