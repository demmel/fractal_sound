/*
 * string_ops.h
 *
 *  Created on: Jun 29, 2013
 *      Author: david
 */

#ifndef STRING_OPS_H_
#define STRING_OPS_H_

#include <list>

bool string_trim(std::string &str);
bool string_reduce(std::string &str);
void print_string_list(const std::list<std::string> &list);

#endif /* STRING_OPS_H_ */
