/*
 * RuleParser.h
 *
 *  Created on: Jun 29, 2013
 *      Author: david
 */

#ifndef FRACTALPATTERNPARSER_H_
#define FRACTALPATTERNPARSER_H_

#include <string>
#include <list>
#include "types.h"

namespace frctl_snd {

class FractalPatternParser {
public:
	FractalPatternParser();
	~FractalPatternParser();
	SymbolSequence& getSequence() { return sequence; }
	SymbolSet& getSymbols() { return symbols; }
	RuleSet& getRules() { return rules; }
	void parseLine(std::string line);
private:
	SymbolSequence sequence;
	SymbolSet symbols;
	RuleSet rules;
};

} /* namespace frctl_snd */

#endif /* FRACTALPATTERNPARSER_H_ */
