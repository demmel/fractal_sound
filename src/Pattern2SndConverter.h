/*
 * Pattern2SndConvertor.h
 *
 *  Created on: Jun 30, 2013
 *      Author: david
 */

#ifndef PATTERN2SNDCONVERTOR_H_
#define PATTERN2SNDCONVERTOR_H_

#include <vector>
#include <map>

#include "FractalPattern.h"

namespace frctl_snd {

typedef std::vector<int> SymbolProperty;
typedef std::map<Symbol, SymbolProperty> SymbolProperties;

class Pattern2SndConverter {
public:
	Pattern2SndConverter(std::string fileName = "");
	virtual ~Pattern2SndConverter();
	int *convert(FractalPattern &pattern);
protected:
	SymbolProperties properties;
private:
	virtual int *convertPattern(FractalPattern &pattern) = 0;
	void readPropertyFile(std::string fileName);
};

class Pattern2SndConverterFactory {
public:
	static Pattern2SndConverter *create(std::string type, std::string prop_file = "");
};

} /* namespace frctl_snd */
#endif /* PATTERN2SNDCONVERTOR_H_ */
