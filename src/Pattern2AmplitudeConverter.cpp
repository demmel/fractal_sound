/*
 * Pattern2FrequencyConvertor.cpp
 *
 *  Created on: Jun 30, 2013
 *      Author: david
 */

#include "Pattern2AmplitudeConverter.h"

namespace frctl_snd {

Pattern2AmplitudeConverter::Pattern2AmplitudeConverter(std::string fileName) :
		Pattern2SndConverter(fileName) {
}

Pattern2AmplitudeConverter::~Pattern2AmplitudeConverter() {
}

int *Pattern2AmplitudeConverter::convertPattern(FractalPattern &pattern) {
	size_t size = 2*pattern.getCurrentSequence().size();
	int *sound = new int[size];

	int pos = 0;
	for(auto it = pattern.getCurrentSequence().begin(); it != pattern.getCurrentSequence().end(); it++) {
		sound[pos] = properties[*it][0];
		sound[pos+1] = properties[*it][0];
		pos += 2;
	}

	return sound;
}

} /* namespace frctl_snd */
